# gitlab_ci_runner_service

[Back to resource list](../README.md#resources)

Manage the Gitlab CI runner service, all settings are optional.

## Actions

- `:start`
- `:stop`
- `:restart`
- `:reload`
- `:enable`
- `:disable`

## Usage

### Example

```ruby
gitlab_ci_runner_service 'gitlab-runner' do
  action [:enable, :start]
end
```

The full resource options list is available in [resources/global_config.rb](../resources/service.rb).
