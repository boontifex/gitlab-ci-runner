# gitlab_ci_runner_runner

[Back to resource list](../README.md#resources)

## Actions

- `:register` - Register a runner
- `:update` - Update settings for a previously registered runner
- `:unregister` - Unregister a runner and remove its configuration

## Properties

### General

| Property               | Optional?       | Type   | Description                                                               |
|------------------------|-----------------|--------|---------------------------------------------------------------------------|
| `url`                  | Register: No    | String | Gitlab server URL.                                                        |
|                        | Unregister: Yes |        |                                                                           |
| `registration_token`   | Register: No    | String | Gitlab server runner registration token.                                  |
|                        | Unregister: Yes |        |                                                                           |
| `administration_token` | Yes             | String | Token with API access for an administrator on the gitlab server.          |
| `config`               | Yes             | String | Gitlab runner configuration file path.                                    |

### Server options

| Property               | Optional?  | Type   | Description                                                                                            |
|------------------------|------------|--------|--------------------------------------------------------------------------------------------------------|
| `description`          | Yes        | String | Server runner description.                                                                             |
| `active`               | Yes        | Bool   | The state of a runner; can be set to `true` or `false`, paused runners don't accept new jobs.          |
| `tag_list`             | Yes        | Array  | The list of tags for a runner; put array of tags, that should be finally assigned to a runner.         |
| `run_untagged`         | Yes        | Bool   | Flag indicating the runner can execute untagged jobs.                                                  |
| `locked`               | Yes        | Bool   | Flag indicating the runner is locked When a runner is locked, it cannot be assigned to other projects. |
| `access_level`         | Yes        | String | The access_level of the runner; `not_protected` or `ref_protected`                                     |
| `maximum_timeout`      | Yes        | Int    | Maximum timeout set when this Runner will handle the job.                                              |

### Local option

| Property               | Optional?  | Type   | Description                                                                                              |
|------------------------|------------|--------|----------------------------------------------------------------------------------------------------------|
| `options`              | Yes        | Hash   | Runner configuration options. <https://docs.gitlab.com/runner/configuration/advanced-configuration.html> |

## Usage

Register or Unregister runners.

### Example 1 - Register new runner

Register a default runner to <http://gitlab-ci.myinstance>.

```ruby
gitlab_ci_runner_runner 'my runner' do
  registration_token '1234567890'
  administrative_token '987654321'
  url 'http://gitlab-ci.myinstance'
  tag_list [ "testing-1", "testing-2" ]
  options({
    'executor' => 'shell'
  })
end
```

### Example 2 - Register and update

Register a runner and update it's configuration once registered. Runners that are already registered will have their configuration updated to match the supplied options. Empty hash values will be compacted (removed).

If an administrative token with suitable permissions has been provided the server runner configuration will also be updated to match the resource.

```ruby
gitlab_ci_runner_runner 'my runner' do
  registration_token '1234567890'
  administrative_token '987654321'
  url 'http://gitlab-ci.myinstance'
  description 'Testing Docker Runner 1'
  options({
    "executor" => "docker",
    "docker" => {
      "image" => "centos",
      "privileged" => false,
      "disable_entrypoint_overwrite" => false,
      "oom_kill_disable" => false,
      "disable_cache" => false,
      "volumes" => [
        "/cache",
        "/etc/chef:/etc/chef:ro"
      ],
      "shm_size" => 0
    },
    "cache" => {
      "s3" => {},
      "gcs" => {}
    },
    "custom_build_dir" => {
      "enabled" => true
    },
    "tag_list" => [
      "docker-test-1"
    ]
  })
  action [ :register, :update ]
end
```

Will generate this configuration:

```toml

[[runners]]
executor = "docker"
name = "Testing Docker Runner 1"
token = "9dK52sYkuXeVudzw3HuH"
url = "http://gitlab-ci.myinstance"

[runners.docker]
disable_cache = false
disable_entrypoint_overwrite = false
image = "centos"
oom_kill_disable = false
privileged = false
shm_size = 0
volumes = ["/cache","/etc/chef:/etc/chef:ro"]

[runners.custom_build_dir]
enabled = true
```

The full resource options list is available in [resources/runner.rb](../resources/runner.rb).
