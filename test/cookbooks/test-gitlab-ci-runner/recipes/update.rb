#
# Copyright:: 2020, Ben Hughes <bmhughes@bmhughes.co.uk>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

gitlab_ci_runner_runner 'test runner create' do
  sensitive false
  url 'http://kitchen-gitlab-ce'
  registration_token '1234567890'
  administration_token 'ABC123'
  description 'Custom description'
  tag_list %w(test_tag new_test_tag)
  locked false
  options(
    'limit' => 5,
    'environment' => [
      'PATH=${PATH}:/opt/chefdk/embedded/bin/',
      'BERKSHELF_PATH=/dev/shm/foo/.berkshelf',
    ],
    'executor' => 'shell'
  )
  retries 1

  action :update
end
