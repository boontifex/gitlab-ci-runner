#
# Copyright:: 2020, Ben Hughes <bmhughes@bmhughes.co.uk>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

gitlab_ci_runner_package '' do
  action :install
end

gitlab_ci_runner_service '' do
  action %i(enable start)
  subscribes :restart, 'template[/etc/gitlab-runner/config.toml]', :delayed
end

gitlab_ci_runner_global_config '' do
  sensitive false
  concurrent 5
  check_interval 3
  listen_address '0.0.0.0'
  session_server(
    'listen_address' => '0.0.0.0:8093',
    'session_timeout' => 1800
  )
end
