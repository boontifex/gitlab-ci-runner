# Upgrading

This document will give you help on upgrading major versions of gitlab-ci-runner.

## v3.0.0

### Added

- load_current_value and converge_if_changed support
  - global resource
  - runner resource

### Changed

- Following properties are now required for the runner resource:
  - `:url`
  - `:registration_token` - Only with the `:register` action
