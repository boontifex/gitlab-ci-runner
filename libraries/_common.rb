#
# Cookbook:: gitlab-ci-runner
# Library:: _common
#
# Copyright:: 2020, Ben Hughes <bmhughes@bmhughes.co.uk>, All Rights Reserved.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

module GitlabCiRunner
  module Cookbook
    # Common helper methods
    module CommonHelpers
      def nil_or_empty?(*values)
        values.any? { |v| v.nil? || (v.respond_to?(:empty?) && v.empty?) }
      end

      def log_chef(level, message)
        Chef::Log.send(level, "#{caller[0][/`.*'/][1..-2]}: #{message}")
      end

      def raise_error(type: RuntimeError, message: nil)
        raise type, message
      end

      def compact_hash(hash)
        p = proc do |*args|
          v = args.last
          v.delete_if(&p) if v.respond_to?(:delete_if)
          v.nil? || v.respond_to?(:empty?) && v.empty?
        end
        hash.delete_if(&p)
      end

      def safe_compare(value1, value2)
        v1 = value1.is_a?(Array) ? value1.sort : value1
        v2 = value2.is_a?(Array) ? value2.sort : value2

        v1.eql?(v2)
      end

      def hash_keys_string?(hash)
        raise ArgumentError, "Expected Hash, got #{hash.class}." unless hash.is_a?(Hash)

        return false unless hash.keys.all? { |k| k.is_a?(String) }
        hash.select { |_, v| v.is_a?(Hash) }.each_value { |v| return false unless hash_keys_string?(v) }

        log_chef(:debug, 'Passed: all keys are String class.')
        true
      end
    end
  end
end
